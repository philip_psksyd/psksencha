(function() {

    Ext.define('PSK.data.Forms', {
        singleton: true,
        requires: [
            'PSK.data.API',
            'PSK.view.grid.ComboBox'
        ],
        constructor: function(config) {
            this.initConfig(config);
        },
        documentURL: function(uri) {

            if (PSK.data.API.getDebug()) {
                // debugging
                return './resources/mocked/document.json';
            }

            return PSK.data.API.getUrls().xappXspBase + 'document?uri=' + encodeURIComponent(uri);
        },
        loadDocument: function(uri, callback) {
            var url = this.documentURL(uri);

            PSK.data.API.request(url, function(response) {
                if (callback) {
                    callback(response);
                }
            });
        },
        designURL: function(database, uri) {

            if (PSK.data.API.getDebug()) {
                // debugging
                switch (uri) {
                    case "luWoByResponsible":
                        return './resources/mocked/piedesign.json';

                    default:
                        return './resources/mocked/design.json';
                }
            }

            database = PSK.data.API.getUrls().basePathModules + database;

            return PSK.data.API.getUrls().viewXspBase + 'design?uri=' + encodeURIComponent(database + "/" + uri);
        },
        entriesURL: function(database, uri) {
            database = PSK.data.API.getUrls().basePathModules + database;
            var url = PSK.data.API.getUrls().viewXspBase + 'entries';
            var params = {
                uri: encodeURIComponent(database + '/' + uri),
                type: 'flat'
            };

            if (PSK.data.API.getDebug()) {
                // debugging
                switch (uri) {
                    case "luWoByResponsible":
                        url = './resources/mocked/pie.json';
                        break;

                    default:
                        url = './resources/mocked/entries.json';
                }
            }

            return {
                url: url,
                params: params
            };
        },
        loadDesign: function(database, uri, callback) {
            var url = this.designURL(database, uri);

            PSK.data.API.request(url, function(response) {
                if (callback) {
                    callback(response);
                }
            });
        },
        getColumnsFromDesign: function(grid, design, params) {
            var columns = [];
            var comboboxes = [];

            if (design && design['@columns']) {
                for (var i = 0; i < design['@columns'].length; i++) {
                    var column = design['@columns'][i];

                    var columnTitle = column['@columnheader'] ? column['@columnheader']['@title'] : '';
                    var itemName = column['@itemname'];

                    columns.push(new Column(column, itemName, columnTitle));

                    // NOTE: In Martins POC he excludes category columns in grid. Do or do not?

                    if (column['@categorized'] === true && (!params.category || i > 0)) {
                        var comboBox = new ComboBox(grid, params, itemName, columnTitle, i === 0 && !params.category);

                        comboboxes.push(comboBox);
                    }
                }
            }

            return {
                columns: columns,
                comboboxes: comboboxes
            };
        }
    });

    function getColumnCustomType(column) {
        if (column.custom) {
            for (var i = 0, l = column.custom.length; i < l; i++) {
                var type = column.custom[i]['generic-xsp-column-type']; //string, int, number, boolean, date or empty string (auto);
                if (type) {
                    return type;
                }
            }
        }
        return ''; //or string?
    }

    function ComboBox(grid, extraParams, itemName, columnTitle, canUseRestictToCategory) {
        return Ext.create('PSK.view.grid.ComboBox', grid, {
            extraParams: {
                uri: extraParams.uri,
                category: extraParams.category || '',
                columns: itemName
            },
            rootProperty: itemName
        }, {
            emptyText: columnTitle,
            _itemName: itemName,
            _grid: grid,
            _canUseRestictToCategory: canUseRestictToCategory
        });
    }

    function Column(column, itemName, columnTitle) {

        var defaultDateFormat = Ext.Date.defaultFormat + ' H:i'; //TODO - get from @datetimeformat ?
        var defaultNumberFormat = '0,000.##'; //TODO - get from @numberformat ? 

        //TODO - usa a Ext.XTemplate instead of a gridcolumn when multi-value?
        //		var xtype=column['@listseparator'] ? 'gridcolumn' : column['@datetimeformat'] ? 'datecolumn' : column['@numberformat'] ? 'numbercolumn' : 'gridcolumn';
        var type = getColumnCustomType(column);
        var xtype = column['@listseparator'] ? 'gridcolumn' : type === 'date' ? 'datecolumn' : type === 'number' ? 'numbercolumn' : 'gridcolumn';

        return {
            dataIndex: itemName,
            text: columnTitle ? columnTitle : String.fromCharCode(160),

            //TODO - this is correct for theme-triton but not for others...
            //width: column['@width'] * 12 + 0 + 10, //dominoWidth*aproxCellFontSize+cellBorderWidth+cellMarginLeft
            // minWidth: 24, //to match the "extjs hover menu button"...
            // autoSizeColumn: true,
            flex: 1,
            xtype: xtype,

            format: xtype == 'numbercolumn' ? defaultNumberFormat : xtype == 'datecolumn' ? defaultDateFormat : undefined,
            align: column['@align'],

            editor: {
                xtype: 'textfield'
            },

            filter: {
                type: xtype == 'datecolumn' ? 'date' : xtype == 'numbercolumn' ? 'number' : 'list'
            }
        };
    }

    // function Field(column) {
    //
    //     var type = getColumnCustomType(column);
    //
    //     return {
    //         name: column['@itemname'],
    //         type: column['@listseparator'] ? '' : type, //empty string to support arrays
    //
    //
    //         convert: column['@showasicons'] ? getIconConverter() :
    //
    //             column['@listseparator'] ? getArrayConverter(column['@listseparator'], type) : type === 'date' || type === 'number' ? function(value) {
    //                 return value instanceof Array ? value[0] : value;
    //             } : function(value) {
    //                 return Ext.util.Format.htmlEncode(value);
    //             }
    //     };
    // }

}());
