(function() {

    Ext.define('PSK.data.User', {
        singleton: true,
        requires: [
            'PSK.data.API'
        ],
        config: {
            session: {},
            name: null,
            server: null,
            // TODO: userType from backend `manager` or `worker` ?
            userType: 'manager'
        },
        constructor: function(config) {
            this.initConfig(config);
        },
        loadSession: function(){
            var url = PSK.data.API.getUrls().xappXspBase + 'session' + window.location.search;
            PSK.data.API.request(url, function(response){
                this.setSession(response);
                this.setName(response.user.name);
                this.setServer(response.server.name);
            }.bind(this));
        },
        isAnonymous: function(){
            return !this.getName() || this.getName() === "Anonymous";
        },
        getLoginURL: function(){
            return PSK.data.API.getServerBase() +
                    PSK.data.API.getPaths().basePathDominoDirectory+
                    '?login&redirectto='+
                    encodeURIComponent(window.location.href);
        },
        getLogoutURL: function(){
            return PSK.data.API.getServerBase() +
                    PSK.data.API.getPaths().basePathDominoDirectory+
                    '?logout&redirectto='+
                    encodeURIComponent(window.location.href);
        },
        sendToLogin: function(){
            window.location.assign(this.getLoginURL());
        },
        logout: function(confirm){
            // make confirm defaults to tru
            if(confirm === undefined){
                confirm = true;
            }

            if(confirm){
                Ext.Msg.confirm('Log out', 'Are you sure you want to logout?', function(button){
                    if(button === "yes"){
                        window.location.replace(this.getLogoutURL());
                    }
                }.bind(this));

                return true;
            }

            window.location.replace(this.getLogoutURL());

            return true;
        }
    });

}());
