(function(forHost) {

    var paths = {
        homeHost: 'okq8.psksyd.com',
        basePathDominoDirectory: '/names.nsf/',
        basePathModules: '/kund/okq8/',
        basePathXAgents: '/kund/gemensam/xagents.nsf/',
        useSSL: false
    };

    var serverBase = (paths.useSSL ? 'https://' : 'http://') + paths.homeHost;

    Ext.define('PSK.data.API', {
        singleton: true,
        config: {
            urls: {
                basePathModules: paths.basePathModules,
                xappXspBase: serverBase + paths.basePathXAgents + 'xapp.xsp/',
                treeXspBase: serverBase + paths.basePathXAgents + 'tree.xsp/',
                viewXspBase: serverBase + paths.basePathXAgents + 'view.xsp/'
            },
            debug: /localhost:[0-9]{4}/.test(forHost),
            paths: paths,
            serverBase: serverBase,
            ssl: paths.useSSL
        },
        constructor: function(config) {
            this.initConfig(config);
        },
        request: function(url, callback){
            Ext.Ajax.request({
                url: url,
                disableCaching: false,
                method: 'GET',
                success: function(response){
                    if(callback){
                        callback( Ext.decode(response.responseText) );
                    }
                }
            });
        }
    });

}(window.location.host));
