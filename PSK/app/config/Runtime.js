(function() {

    Ext.define('PSK.config.Runtime', {
        singleton: true,
        config: {
            viewport: null
        },
        constructor: function(config) {
            this.initConfig(config);
        }
    });

}());
