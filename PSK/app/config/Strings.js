(function() {

    Ext.define('PSK.config.Strings', {
        singleton: true,
        config: {

            clientName: 'PSK Syd AB',
            navigationPanelLabel: 'PSK Navigation',
            navigatorTreeColumnTitle: 'Views',
            homePageLabel: 'PSK Home',
            treeComboBoxDatabaseLabel: 'Database',

            gridSearchActionLabel: 'Search',
            gridSearchActionTooltip: 'Full-Text search (supporting wildcards and logical operators like AND and OR)',

            viewCountLabel: '{0} document(s)',
            viewExportButtonLabel: 'Export',

            //	profileWindowTitle:'Profile - {0}',
            userMenuTitle: 'User options',
            profileButtonLabel: 'User profile',
            profileButtonTooltip: 'Open user profile',
            profileSignOutQuestion: 'Sign out from {0}',
            buttonLabelSignIn: 'Sign in',
            buttonLabelSignOut: 'Sign out',

            gridComboBoxAllSelectText: 'All',

            logoURL: 'http://psksyd.com/wp-content/themes/psksyd/images/site-logo.png'
        },
        format: function() {
            return Ext.String.format.apply(this, arguments);
        },
        constructor: function(config) {
            this.initConfig(config);
        }
    })

}());
