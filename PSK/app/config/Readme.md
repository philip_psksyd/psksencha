# Config Files

This directory is containing config files for paths, urls, client information and strings.

## Usage:

In a controller/viewmodel/view etc:
```javascript

Ext.define('PSK.view.Controller', {
    ...
    requires: [
        ...
        'PSK.config.Runtime'
    ],
    ...
    ...
    onItemClick: function(){
        // The configs has setters and getters for the config data.
        PSK.config.Runtime.getViewport().add(element);
    }
})

```

## Structure

```javascript

(function() {

    Ext.define('PSK.config.Runtime', {
        singleton: true,
        config: {
            viewport: null
        },
        constructor: function(config) {
            this.initConfig(config);
        }
    });

}());


```

##### Functions
Items in the config object of the classes has getters and setters.
(For example; the `viewport` of `PSK.config.Runtime` is accessible with `PSK.config.Runtime.getViewport()` and editable with `setViewport()` ).
