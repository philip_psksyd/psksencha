(function() {

    Ext.define('PSK.store.EntryStore', {
        extend: 'Ext.data.Store',

        autoLoad: false,
        fields: [],
        proxy: {
            type: 'ajax',
            limitParam: '',
            noCache: false,
            pageParam: '',
            startParam: '',
            timeout: 60000,
            reader: {
                type: 'json',
                rootProperty: '@entries'
            }
        }
    });

}());
