(function() {

    Ext.define('PSK.store.NavigationStore', {
        extend: 'Ext.data.TreeStore',
        root: {
            expanded: true,
            children: [
                {
                    text: 'Dashboard',
                    leaf: true,
                    link: 'home'
                },
                {
                    text: 'Documents',
                    leaf: true,
                    database: 'documents.nsf',
                    uri: 'docBySiteIDCurrent'
                },
                {
                    text: 'Anläggningar',
                    expanded: false,
                    database: 'main.nsf',
                    uri: 'UrvalAnlaggning',
                    children: [
                        {
                            text: 'Stängda anläggningar',
                            leaf: true
                        },
                        {
                            text: 'Samtliga fält',
                            leaf: true
                        },
                        {
                            text: 'Sökvy',
                            leaf: true
                        },
                        {
                            text: 'Skapa ny anläggning',
                            leaf: true
                        },
                        {
                            text: 'Anläggningsöversik',
                            leaf: true
                        }
                    ]
                }, {
                    text: 'Översikt',
                    leaf: true
                }
            ]
        }
    });

}());
