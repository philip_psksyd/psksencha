(function() {

    Ext.define('PSK.store.charts.WorkOrdersPieStore', {
        extend: 'Ext.data.Store',
        requires: [
            'PSK.data.Forms',
            'PSK.data.User'
        ],

        constructor: function(config) {
            this._category = config.category || 'ALL';

            this._colMatch = {};

            this._uri = encodeURIComponent(PSK.data.API.getUrls().basePathModules + 'workorder.nsf' + '/' + 'luWoByResponsible');

            var urlObject = PSK.data.Forms.entriesURL('workorder.nsf', 'luWoByResponsible');
            urlObject.params.category = this._category;

            var transform = {
                fn: function(data) {
                    var c = this._colMatch;

                    // TODO: Translation
                    var array = [{
                        status: 'Sena',
                        count: 0
                    }, {
                        status: 'Sena inom 7 dagar',
                        count: 0
                    }, {
                        status: 'Pågående',
                        count: 0
                    }, {
                        status: 'Beställda',
                        count: 0
                    }, {
                        status: 'För behandling',
                        count: 0
                    }];

                    for (var i = 0; i < data['@entries'].length; i++) {
                        var row = data['@entries'][i];
                        var now = new Date();
                        var dueTime = null;
                        var dueTime7 = null;

                        if (row[c['ordered']].trim()) {
                            // if ordered

                            if (row[c['due-by']].trim()) {

                                var due = row[c['due-by']].trim();
                                dueTime = new Date(due);
                                dueTime7 = new Date(due);
                                dueTime7.setDate(dueTime7.getDate() - 7);

                                if (dueTime.getTime() < now.getTime()) {
                                    // late
                                    array[0].count++;

                                } else {
                                    // check if 7 days to late
                                    if (dueTime7.getTime() < now.getTime()) {
                                        // late within 7 days
                                        array[1].count++;
                                    }

                                }

                            }

                            if (row[c['accept']].trim()) {
                                // if ordered and accepted

                                array[2].count++;

                            } else {

                                if (PSK.data.User.getUserType() === 'worker') {
                                    array[4].count++;
                                } else {
                                    array[3].count++;
                                }

                            }

                        } else {
                            // not ordered yet
                            array[4].count++;
                        }

                    }

                    return array;
                },
                scope: this
            };

            Ext.applyIf(config, {
                proxy: {
                    url: urlObject.url,
                    extraParams: urlObject.params,
                    type: 'ajax',
                    limitParam: '',
                    noCache: false,
                    pageParam: '',
                    startParam: '',
                    timeout: 60000,
                    reader: {
                        type: 'json',
                        rootProperty: '@entries',
                        transform: transform
                    }
                }
            });

            this.callParent([config]);

            PSK.data.Forms.loadDesign('workorder.nsf', 'luWoByResponsible', function(design) {

                var match = {};

                for (var i = 0; i < design['@columns'].length; i++) {
                    var col = design['@columns'][i];
                    if (col['@columnheader']) {
                        // match[col['@itemname']] = col['@columnheader']['@title'];
                        match[col['@columnheader']['@title']] = col['@itemname'];
                    }
                }

                this._colMatch = match;

                this.load();

            }.bind(this));
        },

        fields: [
            'status', 'count'
        ],
        autoLoad: false,
        proxy: null
    });

}());
