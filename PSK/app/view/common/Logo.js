(function() {

    Ext.define('PSK.view.common.Logo', {
        extend: 'Ext.Img',
        requires: [
            'PSK.config.Strings'
        ],

        alt: PSK.config.Strings.getClientName(),
        src: PSK.config.Strings.getLogoURL()
    });

}());
