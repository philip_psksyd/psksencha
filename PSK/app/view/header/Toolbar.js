(function() {

    Ext.define('PSK.view.header.Toolbar', {
        extend: 'Ext.toolbar.Toolbar',
        requires: [
            'PSK.view.header.UserButton',
            'PSK.view.header.ExternalLink'
        ],

        layout: 'hbox',
        defaultButtonUI: 'default',

        items: [
            Ext.create('PSK.view.header.ExternalLink', {
                text: 'LIS / Curves',
                menu: {
                    title: {
                        xtype: 'panel',
                        title: 'LIS / Curves',
                        tools: [{
                            type: 'help'
                        }],
                        html: 'Beskrivning'
                    },
                    minWidth: 200,
                    items: [
                        {
                            text: 'LIS',
                            iconCls: 'flag flag-dk'
                        },
                        {
                            text: 'Curves',
                            iconCls: 'flag flag-se'
                        }
                    ]
                }
            }),
            {
                xtype: 'tbspacer',
                width: 5
            },
            Ext.create('PSK.view.header.ExternalLink', {
                text: 'OKQ8 Klima',
                menu: {
                    title: {
                        xtype: 'panel',
                        title: 'OKQ8 Klima',
                        tools: [{
                            type: 'help'
                        }],
                        html: 'Beskrivning'
                    },
                    minWidth: 200,
                    items: [
                        {
                            text: 'www.q8klima.dk',
                            iconCls: 'flag flag-dk'
                        },
                        {
                            text: 'www.okq8klima.se',
                            iconCls: 'flag flag-se'
                        }
                    ]
                }
            }),
            {
                xtype: 'tbspacer',
                width: 5
            },
            Ext.create('PSK.view.header.ExternalLink', {
                text: 'Allmänt',
                menu: {
                    title: {
                        xtype: 'panel',
                        title: 'Allmänt',
                        tools: [{
                            type: 'help'
                        }]
                    },
                    minWidth: 300,
                    items: [
                        {
                            iconCls: 'x-fa fa-external-link',
                            text: 'Stationsdatabase'
                        },
                        {
                            iconCls: 'x-fa fa-external-link',
                            text: 'Autovärkstadsbekendtgörelsen'
                        },
                        {
                            iconCls: 'x-fa fa-external-link',
                            text: 'Olietankbekendtgörelsen'
                        },
                        {
                            iconCls: 'x-fa fa-external-link',
                            text: 'Benzinstationsbekendtgörelsen'
                        },
                        {
                            iconCls: 'x-fa fa-external-link',
                            text: 'Tankdatabase'
                        },
                        {
                            iconCls: 'x-fa fa-external-link',
                            text: 'OKQ8 Sikkerhedsbekendtgörelser'
                        }
                    ]
                }
            }),
            {
                xtype: 'tbspacer',
                width: 20
            },
            Ext.create('PSK.view.header.UserButton')
        ]

    });

}());
