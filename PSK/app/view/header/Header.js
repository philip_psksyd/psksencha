(function() {

    var headerHeight = 64;

    Ext.define('PSK.view.header.Header', {
        extend: 'Ext.panel.Panel',
        requires: [
            'PSK.view.header.Toolbar',
            'PSK.view.common.Logo'
        ],

        id: 'main-header',
        flex: 0,
        height: headerHeight,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [
            Ext.create('PSK.view.common.Logo', {
                maxHeight: headerHeight,
                width: 'auto',
                height: '100%',
                margin: 10
            })
        ],
        dockedItems: [
            Ext.create('PSK.view.header.Toolbar', {
                dock: 'right'
            })
        ]
    });

}());
