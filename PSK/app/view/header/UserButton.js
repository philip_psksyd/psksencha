(function() {

    Ext.define('PSK.view.header.UserButton', {
        extend: 'Ext.button.Button',
        requires: [
            'PSK.data.User',
            'PSK.config.Strings'
        ],
        text: !PSK.data.User.isAnonymous() ? PSK.data.User.getName() : PSK.config.Strings.getButtonLabelSignIn(),
        iconCls: 'x-fa ' + (PSK.data.User.isAnonymous() ? 'fa-sign-in' : 'fa-user'),
        // if user is not logged in send to login page on button click
        listeners: !PSK.data.User.isAnonymous() ? undefined : {
            click: function() {
                PSK.data.User.sendToLogin();
            }
        },
        // if logged out show no menu
        menu: PSK.data.User.isAnonymous() ? undefined : {
            title: {
                xtype: 'panel',
                title: PSK.config.Strings.getUserMenuTitle(),
                tools: [{
                    type: 'help'
                }]
            },
            minWidth: 200,
            items: [{
                text: PSK.config.Strings.getProfileButtonLabel(),
                iconCls: 'x-fa fa-gear',
                disabled: true
            }, {
                text: PSK.config.Strings.getButtonLabelSignOut(),
                iconCls: 'x-fa fa-sign-out',
                handler: function() {
                    PSK.data.User.logout();
                }
            }]
        }

    });

}());
