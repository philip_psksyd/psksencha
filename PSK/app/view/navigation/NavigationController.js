(function() {

    Ext.define('PSK.view.navigation.NavigationController', {
        extend: 'Ext.app.ViewController',
        alias: 'controller.navigation',

        onItemClick: function(view, record) {
            if (record && record.data) {
                if (record.data.database && record.data.uri) {
                    
                    this.redirectTo(
                        'list/' +
                        encodeURIComponent(record.data.database) + '/' +
                        encodeURIComponent(record.data.uri)
                    );

                } else if (record.data.link) {
                    this.redirectTo(record.data.link);
                }
            }
        }
    });

}());
