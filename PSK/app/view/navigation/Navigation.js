(function() {

    /**
    The navigation tree component to be used in views.
    */

    Ext.define('PSK.view.navigation.Navigation', {
        extend: 'Ext.tree.Panel',

        controller: 'navigation',

        requires: [
            'PSK.view.navigation.NavigationController',

            'PSK.store.NavigationStore',
            'PSK.config.Strings',
            'PSK.data.User',

            'Ext.list.Tree'
        ],
        itemId: 'main-navigation',
        id: 'main-navigation',
        width: 260,
        rootVisible: false,
        border: true,
        resizable: true,
        resizeHandles: 'e',
        bind: {
            title: PSK.config.Strings.getNavigationPanelLabel()
        },
        store: Ext.create('PSK.store.NavigationStore'),
        listeners: {
            itemclick: 'onItemClick'
        },
        dockedItems: [
            
        ]
    });

}());
