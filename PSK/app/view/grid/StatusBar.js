(function() {

    Ext.define('PSK.view.grid.StatusBar', {
        extend: 'Ext.toolbar.Toolbar',
        requires: [

        ],
        items: [
            {
                xtype: 'tbtext',
                bind: '{statusBarText}'
            },
            '->',
            {
            text: PSK.config.Strings.getViewExportButtonLabel(),
            disabled: true,
            listeners: {
                click: function() {
                    // TODO: Export function
                    Ext.Msg.show({
                        title: 'Export',
                        msg: 'TODO',
                        modal: true,
                        icon: Ext.Msg.INFO,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        }]
    });

}());
