(function() {

    Ext.define('PSK.view.grid.DocumentWindow', {
        extend: 'Ext.Window',
        constrain: Ext.isIE,
        requires: [
            'PSK.data.API'
        ],
        constructor: function(config) {
            var privates = {};

            privates.iframeComponent = Ext.create('Ext.Component', {
                border: false,
                autoEl: {
                    width: '100%',
                    tag: 'iframe'
                }
            });

            var panel = Ext.create('Ext.form.Panel', {
                width: 800,
                height: 600,
                frame: false,
                layout: 'fit',
                items: privates.iframeComponent,
                bodyBorder: true
            });

            Ext.applyIf(config, {
                maximizable: true,
                layout: 'fit',
                items: panel,
                onEsc: Ext.emptyFn,
                privates: privates,
                tools: [{
                    xtype: 'tool',
                    type: 'refresh',
                    listeners: {
                        click: function() {

                            var iframe = getIframeDom.call(this);
                            if (iframe) {
                                iframe.contentWindow.location.reload(false);
                            }

                        }.bind(this)
                    }
                }]
            });

            this.callParent([config]);
        },
        load: function(url) {
            window.setTimeout(function() {

                var iframe = getIframeDom.call(this);
                if (iframe) {
                    iframe.contentWindow.location.replace(url);
                }

            }.bind(this), 100);
        },
        show: function() {
            this.callParent(arguments);

            window.setTimeout(function() {
                var iframe = getIframeDom.call(this);
                if (iframe) {
                    initIframeContentHandling.call(this, iframe);
                }
            }.bind(this), 100);
        }
    });

    /**
    Iframe manipulating functions
    */

    function getIframeDom() {
        var el = Ext.getDom(this.config.privates.iframeComponent.el);
        return el;
    }

    function messageToString(msg) {
        return msg === undefined ? 'undefined' : msg === null ? 'null' : msg.toString();
    }

    function initIframeContentHandling(iframe) {
        var parent = this;
        iframe.onload = function() {

            parent.setTitle(this.contentWindow.document.title);

            this.contentWindow.eval('var parent=window;'); //IE11 does not support doing it like this: this.contentWindow.parent=this.contentWindow

            this.contentWindow.eval('var opener=window.opener={closed:true};');

            // replace window.open function to open a sencha window instead
            this.contentWindow.open = function(url, target, options) {
                // TODO: Test this
                var o = {};

                if (options) {
                    options = options.split(',');
                    for (var i = 0; i < options.length; i++) {
                        var a = options[i].split('=');
                        o[a[0].trim()] = a[1].trim();
                    }
                }

                var cfg = {};
                if (o.left) cfg.x = parseInt(o.left);
                if (o.top) cfg.y = parseInt(o.top);
                if (o.width) cfg.width = parseInt(o.width);
                if (o.height) cfg.height = parseInt(o.height);

                var win = Ext.create('PSK.view.grid.DocumentWindow', cfg);
                win.load((url && url.indexOf('/') == 0 ? PSK.data.API.getServerBase() : '') + url);
                win.show();

                return win.getDomWindow();
            };

            this.contentWindow.close = function() {
                parent.close();
            };

            this.contentWindow.alert = function(msg, title, callback) {
                if (callback) {
                    Ext.Msg.alert(messageToString(title), messageToString(msg), callback, this);
                } else {
                    alert(msg);
                }
            };
            this.contentWindow.confirm = function(msg, title, callback) {
                if (callback) {
                    Ext.Msg.confirm(
                        messageToString(title),
                        messageToString(msg),
                        callback,
                        this
                    );
                } else {
                    return confirm(msg);
                }

                return false;
            };
            this.contentWindow.prompt = function(msg, title, callback, value) {
                if (callback) {
                    Ext.Msg.prompt(
                        messageToString(title),
                        messageToString(msg),
                        callback,
                        this,
                        false,
                        value
                    );
                } else {
                    return prompt(msg, value);
                }

                return null;
            };

            this.contentWindow.onfocus = function() {
                parent.toFront(true); //firefox need this one
                parent.focus();
            };

        };
    }

}());
