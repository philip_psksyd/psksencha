(function() {

    Ext.define('PSK.view.grid.GridController', {
        extend: 'Ext.app.ViewController',
        alias: 'controller.grid',
        requires: [
            'PSK.config.Strings',
            'PSK.data.Forms',
            'PSK.data.API',
            'PSK.view.grid.DocumentWindow'
        ],

        onItemDblClick: function(view, record) {
            var params = this.getView().config.params;
            var gridUri = params.database + '/' + params.uri;

            if (record.data['@id']) {
                var uri = '/' + (record.data['@view'] ? record.data['@view'] : gridUri.split(',')[0]) + '/' + record.data['@id'].split('.')[1] + '?opendocument';

                PSK.data.Forms.loadDocument(uri, function(response){
                    var win = Ext.create('PSK.view.grid.DocumentWindow', {
                        title: response.title || this.getView().title,
                        width: response.width || undefined,
                        height: response.height || undefined
                    });
                    var winUrl = PSK.data.API.getServerBase() + (response.uri || uri);
                    winUrl = PSK.data.API.getServerBase() + '/documents.nsf/docbysiteidcurrent/3ad867a153f42414c1257bc90040e770?editdocument';
                    win.load(winUrl);
                    win.show();
                }.bind(this));
            }
        }

    });

}());
