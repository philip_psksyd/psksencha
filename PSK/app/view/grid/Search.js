(function() {

    var timeoutId;

    Ext.define('PSK.view.grid.Search', {
        extend: 'Ext.form.field.Text',
        requires: [
            'PSK.config.Strings'
        ],
        constructor: function(store, config) {

            config.listeners = {
                change: function(e, text) {
                    if(!store){
                        // no store available
                        return;
                    }

                    text = text.trim();

                    window.clearTimeout(timeoutId);
                    timeoutId = window.setTimeout(function() {

                        if (text !== store.proxy.extraParams.search) {
                            if (text) {
                                store.proxy.extraParams.search = text;
                            } else {
                                delete store.proxy.extraParams.search;
                            }

                            store.load();
                        }

                    }.bind(this), 500);

                }.bind(this)
            };

            this.callParent([config]);
        },

        emptyText: PSK.config.Strings.getGridSearchActionLabel()
    });

}());
