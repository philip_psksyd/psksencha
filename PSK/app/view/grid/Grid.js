(function() {

    Ext.define('PSK.view.grid.Grid', {
        extend: 'Ext.grid.Panel',
        requires: [
            'PSK.view.grid.GridModel',
            'PSK.view.grid.GridController',
            'PSK.store.EntryStore',
            'PSK.data.Forms',
            'PSK.view.grid.StatusBar',
            'PSK.view.grid.Search'
        ],
        viewModel: {
            type: 'grid'
        },
        controller: 'grid',
        constructor: function(design, params, config) {
            if (params && params.database && params.uri) {

                var urlObject = PSK.data.Forms.entriesURL(params.database, params.uri);
                // add store with correct proxy if possible
                config.store = Ext.create('PSK.store.EntryStore', {
                    autoLoad: true,
                    autoDestroy: true,
                    proxy: {
                        url: urlObject.url,
                        extraParams: urlObject.params
                    },
                    listeners: {
                        datachanged: function(data) {
                            var number = Number(data.getCount()).toLocaleString();
                            var text = PSK.config.Strings.format(PSK.config.Strings.getViewCountLabel(), number);

                            this.viewModel.setData({
                                statusBarText: text
                            });
                        }.bind(this)
                    }
                });
            }

            var columnDesign = PSK.data.Forms.getColumnsFromDesign(this, design, urlObject.params);

            config.columns = columnDesign.columns;

            var dockedTopItems = columnDesign.comboboxes;

            var searchField = Ext.create('PSK.view.grid.Search', config.store, {});
            dockedTopItems.push(searchField);

            config.dockedItems = [{
                    dock: 'top',
                    xtype: 'toolbar',
                    items: dockedTopItems
                },
                Ext.create('PSK.view.grid.StatusBar', {
                    itemId: 'view-status',
                    dock: 'bottom'
                })
            ];

            config.params = params;

            this.callParent([config]);
        },
        initComponent: function() {
            this.callParent(arguments);
        },
        listeners: {
            itemdblclick: 'onItemDblClick'
        }
    });

}());
