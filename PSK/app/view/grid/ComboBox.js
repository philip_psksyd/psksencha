(function() {

    Ext.define('PSK.view.grid.ComboBox', {
        extend: 'Ext.form.field.ComboBox',
        requires: [
            'PSK.config.Strings',
            'PSK.data.API'
        ],

        constructor: function(grid, proxyConfig, config) {

            var debugVersion = '';

            switch(config._itemName){
                case '$23':
                    debugVersion = '2';
                    break;
                case '$24':
                    debugVersion = '3';
                    break;
                default:
                    debugVersion = '';
            }

            config.store = {
                autoDestroy: true,
                autoLoad: true,
                fields: [
                    'value', {
                        name: 'name',
                        sortType: 'asUCString'
                    }
                ],
                proxy: {
                    type: 'ajax',

                    // debugging url
                    url: './resources/mocked/values' + debugVersion + '.json',
                    extraParams: proxyConfig.extraParams,
                    // url: PSK.data.API.getUrls().viewXspBase + 'values',

                    limitParam: '',
                    noCache: false,
                    pageParam: '',
                    startParam: '',
                    timeout: 60000,
                    reader: {
                        type: 'json',
                        rootProperty: proxyConfig.rootProperty,
                        transform: function(data) {

                            return data[this.getRootProperty()].map(function(val) {
                                return {
                                    name: val,
                                    value: val
                                };
                            });
                        }
                    }
                }
            };

            this.callParent([config]);
        },

        queryMode: 'local',
        displayField: 'name',
        valueField: 'value',
        autoSelect: false,
        editable: false,
        store: null,
        listConfig: {
            getInnerTpl: function() {
                return '<div data-qtip="{name}" class="psk-combo-list-item-content">{name}</div>';
            }
        },
        listeners: {
            expand: function() {
                if (this.getValue()) {
                    this.store.insert(0, {
                        name: ' - ' + PSK.config.Strings.getGridComboBoxAllSelectText() + ' - ',
                        value: ''
                    });
                }
            },
            collapse: function() {
                var option = this.store.getAt(0);
                if (option && !option.data.value) {
                    this.store.removeAt(0, 1);
                }
            },
            change: function(view, newValue, oldValue) {

                var grid = view._grid;

                var filters;
                var i;

                if (this._canUseRestictToCategory) {

                    if (newValue) {
                        grid.store.proxy.extraParams.category = newValue;
                        grid.store.load();
                    } else {
                        this.clearValue();
                        if (oldValue) {
                            delete grid.store.proxy.extraParams.category;
                            grid.store.load();
                        }
                    }
                } else if (newValue) {

                    filters = !grid.store.proxy.extraParams.filters ? [] : grid.store.proxy.extraParams.filters.split(',');

                    for (i = filters.length - 1; i >= 0; i--) {
                        if (filters[i].split('=')[0] === this._itemName) {
                            filters.splice(i, 1);
                        }
                    }

                    filters.push(this._itemName + '=' + newValue);
                    grid.store.proxy.extraParams.filters = filters.join(',');
                    grid.store.load();

                } else {

                    this.clearValue();
                    if (oldValue) {

                        filters = !grid.store.proxy.extraParams.filters ? [] : grid.store.proxy.extraParams.filters.split(',');

                        for (i = filters.length - 1; i >= 0; i--) {
                            if (filters[i].split('=')[0] === this._itemName) {
                                filters.splice(i, 1);
                            }
                        }

                        if (filters.length > 0) {
                            grid.store.proxy.extraParams.filters = filters.join(',');
                        } else {
                            delete grid.store.proxy.extraParams.filters;
                        }

                        grid.store.load();
                    }
                }

            }
        }
    });

}());
