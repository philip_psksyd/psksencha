(function() {

    Ext.define('PSK.view.dashboard.Dashboard', {
        extend: 'Ext.panel.Panel',
        requires: [
            // 'PSK.view.dashboard.Title',
            'PSK.view.dashboard.chart.WorkingOrdersPie'
        ],

        title: 'Dashboard',

        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'dashboard-wo-pie'
                    },
                    {
                        xtype: 'panel',
                        // title: 'Instrument',
                        html: 'Diagram 2',
                        flex: 1
                    }
                ],
                html: 'test',
                height: 300
            }, {
                xtype: 'panel',
                html: 'test2',
                flex: 1
            }
        ]
    });

}());
