(function() {

    var store = Ext.create('PSK.store.charts.WorkOrdersPieStore', {});

    var pieColors =  [
        '#DD431F',
        '#FFE20A',
        '#0FFF20',
        '#EEEEEE',
        '#6072B2'
    ];

    Ext.define('PSK.view.dashboard.chart.WorkingOrdersPie', {

        extend: 'Ext.panel.Panel',
        requires: [
            'PSK.store.charts.WorkOrdersPieStore',
            'Ext.chart.PolarChart',
            'Ext.chart.series.Pie'
        ],
        autoDestroy: false,
        xtype: 'dashboard-wo-pie',
        alias: 'dashboard-wo-pie',

        cls: 'dashboard-panel',
        title: 'Arbetsordrar',
        flex: 1,
        itemId: 'dashboard-wo-pie',

        layout: {
            type: 'hbox',
            align: 'stretch'
        },

        items: [{
            xtype: 'grid',
            flex: 1,
            rowLines: false,
            columns: [{
                text: 'Status',
                dataIndex: 'status',
                sortable: false,
                hideable: false,
                flex: 1,
                renderer: function(value, metaData, record, row){
                    var color = pieColors[row];

                    metaData.style = 'border-left: 5px solid ' + color;

                    return value;
                }
            }, {
                text: 'Count',
                sortable: false,
                hideable: false,
                dataIndex: 'count',
                flex: 0
            }],
            store: store
        }, {
            width: '100%',
            height: 400,
            flex: 1,
            xtype: 'polar',
            store: store,

            listeners: {
                
            },

            series: {
                type: 'pie',
                angleField: 'count',
                colors: pieColors,
                label: false,
                // showMarkers: false,
                // showInLabel: false,
                donut: 30
            }
        }]

    });

}());
