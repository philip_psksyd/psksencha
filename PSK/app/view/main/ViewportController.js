Ext.define('PSK.view.main.ViewportController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.viewport',
    requires: [
        'PSK.data.API',
        'PSK.data.Forms',

        // Pages
        'PSK.view.dashboard.Dashboard',
        'PSK.view.grid.Grid'
    ],

    routes: {
        'home': 'onHome',
        'list/:database/:form': {
            action: 'onForm',
            conditions: {
                ':database': '([%a-zA-Z\\.\\-\\_,]+)',
                ':form': '([%a-zA-Z\\.\\-\\_,]+)'
            }
        }
    },

    onForm: function(database, uri) {

        this.loadEntries(database, uri, function(design) {
            var title = !design['@name'] ? '' : design['@name'].split('_').join('').split('\\').join(' \u276F ');
            if (!title) title = String.fromCharCode(160);

            var grid = Ext.create('PSK.view.grid.Grid', design, {
                database: database,
                uri: uri
            }, {
                flex: 1,
                region: 'center',
                itemId: 'main-content',
                title: title
                // frame: false
            });

            this.replaceMainContent(grid);

        }.bind(this));

    },

    onHome: function() {

        var dashboard = Ext.create('PSK.view.dashboard.Dashboard', {
            region: 'center',
            flex: 1,
            itemId: 'main-content'
        });

        this.replaceMainContent(dashboard);
    },

    loadEntries: function(database, uri, callback) {
        PSK.data.Forms.loadDesign(database, uri, function(design) {
            if (callback) {
                callback(design);
            }
        });
    },

    replaceMainContent: function(view) {
        if (!view) return false;

        var currentContent = Ext.ComponentQuery.query("#main-content")[0];
        var parent = currentContent.up();

        if (currentContent && parent) {
            parent.remove(currentContent, true);

            parent.add(view);
        }
    }

});
