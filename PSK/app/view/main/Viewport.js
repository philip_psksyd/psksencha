
Ext.define('PSK.view.main.Viewport',{
    extend: 'Ext.panel.Panel',

    requires: [
        'PSK.view.main.ViewportController',
        'PSK.view.main.ViewportModel',

        'PSK.view.navigation.Navigation',
        'PSK.view.header.Header'
    ],

    controller: 'viewport',
    viewModel: {
        type: 'viewport'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        Ext.create('PSK.view.header.Header'),
        {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            itemId: 'body',
            id: 'body',
            flex: 1,
            items: [
                Ext.create('PSK.view.navigation.Navigation', {
                    // custom settings
                    region: 'west'
                }),
                {
                    xtype: 'panel',
                    flex: 1,
                    itemId: 'main-content',
                    region: 'center',
                    html: "Loading..."
                }
            ]
        }
    ]
});
